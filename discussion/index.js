document.querySelector('#txt-first-name')

/* 
    1. document - refers to the whole document
    2. querySelector 
        - used to select specific element as long as it is inside the HTML tag.
        - takes a string input formatted like CSS selector
        - can select elements regardless if the element is id, class, or tag
*/

const firstname = document.querySelector('#txt-first-name')
const lastname = document.querySelector('#txt-last-name')
const fullname = document.querySelector('#span-full-name')

firstname.addEventListener('keyup', event => {
    fullname.innerHTML = firstname.value
})

firstname.addEventListener('keyup', event => {
    console.log(event.target)
    console.log(event.target.value)
})

lastname.addEventListener('keyup', event => {
    fullname.innerHTML = lastname.value
})

/* 
    event 
        - actions that the user is doing in our website
        - includes scroll, click, hover, keypress/type

    addEventListener
        - function that lets the webpage listen to the events by the user
        - takes two argument: (1) Event and (2) function
*/