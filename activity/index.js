const firstname = document.querySelector('#txt-first-name')
const lastname = document.querySelector('#txt-last-name')
const fullname = document.querySelector('#span-full-name')

let username = ""
function printFullName() {
    username = `${firstname.value} ${lastname.value}`
    fullname.innerHTML = username
}

firstname.addEventListener('keyup', printFullName)
lastname.addEventListener('keyup', printFullName)